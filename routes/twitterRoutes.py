from flask import Blueprint

# Import Facebook e instanciando la clase
from controllers.TwitterController import TwitterController
twitter = TwitterController()

# Registrando Blueprint
twitterBP = Blueprint('twitterBP', __name__)

@twitterBP.route('/')
def hola():
    return twitter.hola()
