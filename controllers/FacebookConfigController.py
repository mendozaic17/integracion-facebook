from flask import request

from utils.sendRequests import SendRequests

# Clase que administra los metodos para manipular la configuración de Facebook
class FacebookConfigController(SendRequests):

    def validateProperty(self, property):
        # Estas son las propiedad admitidas
        propertiesAllowed = ['get_started', 'greeting', 'ice_breakers', 'persistent_menu']

        if property not in propertiesAllowed:
            return None

        return property.lower()

    def createProperties(self):
        property = self.validateProperty(request.get_json()['property'])

        if property is None:
            return 'La propiedad enviada no esta permitida'
        else:
            data = self.templateCreateProperties(request.get_json())
            return self.post('facebook', 'me/messenger_profile', data)

    def deleteProperties(self):
        property = self.validateProperty(request.get_json()['property'])

        if property is None:
            return 'La propiedad enviada no esta permitida'
        else:
            data = self.templateDeleteProperties(property)
            return self.delete('facebook', 'me/messenger_profile', data)

    def templateCreateProperties(self, data):
        property = data['property']

        if property == 'get_started':
            payload = data['payload']
            return {
                "get_started":{
                    "payload": payload
                }
            }

        if property == 'greeting':
            message = data['message']
            return {
                "greeting": [
                    {
                        "locale":"default",
                        "text": message
                    }
                ]
            }

        if property == 'ice_breakers':
            breakers = data['breakers']
            return {
                "ice_breakers": breakers
            }

        if property == 'persistent_menu':
            actions = data['actions']
            return {
                "persistent_menu": [
                    {
                        "locale": "default",
                        "composer_input_disabled": False,
                        "call_to_actions": actions
                    }
                ]
            }

    def templateDeleteProperties(self, property):
        options = [ property ]

        # Por regla de facebook, si se necesita eliminar el botón inicio es necesario
        # eliminar el menu persistente
        if property == 'get_started':
            options = ['persistent_menu', 'get_started']

        return {
            "fields": options
        }